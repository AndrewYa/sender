package com.andrew_ya.sender

import java.util.UUID

import akka.NotUsed
import akka.event.{Logging, LoggingAdapter}
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.Attributes
import akka.stream.scaladsl.{Broadcast, Flow, Keep, Sink, Source}
import com.twitter.bijection.Injection
import com.twitter.bijection.avro.GenericAvroCodecs
import org.apache.avro.Schema
import org.apache.avro.generic.{GenericData, GenericRecord}
import org.apache.kafka.clients.producer.ProducerRecord

import scala.collection.JavaConverters._
import scala.concurrent.duration._


object SerializerFlow {


  val schemaString =
    """
      |{
      |    "namespace": "kafka-avro.severstal",
      |     "type": "record",
      |     "name": "event",
      |     "fields":[
      |         {  "name": "id", "type": "string"},
      |         {   "name": "data",  "type": "string"},
      |         {   "name": "timestamp",  "type": "long"},
      |         {   "name": "description", "type": ["string", "null"]},
      |         {   "name": "arr", "type": {"type": "array", "items": "int"}}
      |     ]
      |}""".stripMargin

  val schema: Schema = new Schema.Parser().parse(schemaString)
  val recordInjection: Injection[GenericRecord, Array[Byte]] = GenericAvroCodecs.toBinary(schema)

  def serialize = Flow[Event].map(event => {

    val genericEvent: GenericData.Record = new GenericData.Record(schema)

    genericEvent.put("id", event.id)
    genericEvent.put("data", event.data)
    genericEvent.put("timestamp", event.timestamp)
    genericEvent.put("description", event.description.orNull)
    genericEvent.put("arr", event.array.asJava)

    recordInjection.apply(genericEvent)

  })


  def topic(implicit log: LoggingAdapter) = Flow[Array[Byte]]
    .map(message => new ProducerRecord[Array[Byte], Array[Byte]]("test", message))
    .log("before-serialize")
    .withAttributes(Attributes.logLevels(onElement = Logging.DebugLevel))

  def eventGen(implicit log: LoggingAdapter) =
    Source.tick(initialDelay = 1 second, interval = 1 second, NotUsed)
      .map(_ => Event(System.currentTimeMillis()))
      .log("generate event")
      .withAttributes(Attributes.logLevels(onElement = Logging.DebugLevel))


  def statsSink(implicit log: LoggingAdapter) = Flow[ProducerRecord[Array[Byte], Array[Byte]]]
    .zipWithIndex.filter(_._2 % 30 == 0)
    .log("sended", m => s"${m._2} events sended").withAttributes(Attributes.logLevels(onElement = Logging.InfoLevel))
    .to(Sink.ignore)

  def kafkaSink(producerSettings: ProducerSettings[Array[Byte], Array[Byte]]) = Producer.plainSink(producerSettings)


  def loggedSink(producerSettings: ProducerSettings[Array[Byte], Array[Byte]])(implicit log: LoggingAdapter) =
     Sink.combine(kafkaSink(producerSettings), statsSink)(Broadcast[ProducerRecord[Array[Byte], Array[Byte]]](_))


}

case class Event(id: String, data: String, timestamp: Long, description: Option[String], array: Seq[Int])

object Event {
  def apply(timestamp: Long): Event = {

    val random = scala.util.Random

    val desc = if (random.nextDouble < 0.5) Some("some desc") else None

    val randomNumbers = for (i <- 1 to random.nextInt(10) + 1) yield random.nextInt(100)

    new Event(UUID.randomUUID().toString, "some data", timestamp, desc, randomNumbers)
  }
}