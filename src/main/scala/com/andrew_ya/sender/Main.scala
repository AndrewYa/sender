package com.andrew_ya.sender

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.kafka.ProducerSettings
import akka.stream.ActorMaterializer
import com.andrew_ya.sender.SerializerFlow._
import com.typesafe.config.ConfigFactory
import org.apache.kafka.common.serialization.ByteArraySerializer



object Main {

  val conf = ConfigFactory.load

  implicit val system = ActorSystem("severstal-system", conf)

  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher


  val producerSettings = ProducerSettings(system, new ByteArraySerializer, new ByteArraySerializer)
    .withBootstrapServers(conf.getConfig("akka.kafka.producer").getString("bootstrap-servers"))


  implicit val adapter: LoggingAdapter = Logging(system, "stream logger")

  def main(args: Array[String]): Unit = {


    val done = eventGen
      .via(serialize)
      .via(topic)
      .runWith(loggedSink(producerSettings))

  }


}




