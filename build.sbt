lazy val root = (project in file(".")).
  settings(
    name := "sender",
    version := "1.0",
    scalaVersion := "2.11.11",
    mainClass in Compile := Some("com.andrew_ya.sender.Main")
  )


val akkaVersion = "2.5.3"

libraryDependencies ++= Seq(

  "com.typesafe.akka"       %%  "akka-actor"                     % akkaVersion,
  "com.typesafe.akka"       %%  "akka-stream"                    % akkaVersion,
  "com.typesafe.akka"       %%  "akka-slf4j"                     % akkaVersion,
  "com.typesafe.akka"       %%  "akka-testkit"                   % akkaVersion   % "test",
  "com.typesafe.akka"       %%  "akka-http"                      % "10.0.9",
  "com.typesafe.akka"       %%  "akka-http-spray-json"           % "10.0.9",
  "org.scalatest"           %%  "scalatest"                      % "3.0.0"       % "test",

  "org.apache.kafka" % "kafka_2.11" % "0.10.2.1"  exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12"),
  "com.typesafe.akka" %% "akka-stream-kafka" % "0.16",

  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "org.apache.avro"  %  "avro"  %  "1.8.2",
  "com.twitter" % "bijection-avro_2.11" % "0.9.5" exclude("commons-logging","commons-logging")

)

assemblyMergeStrategy in assembly := {
  case PathList("org", "slf4j", xs@_*) => MergeStrategy.last
  case x => (assemblyMergeStrategy in assembly).value(x)
}
